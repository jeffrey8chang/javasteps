import java.util.List;
import java.util.ArrayList;

public class DecimalToHex {
	public static void main (String args[]) {
		//int  decimal = 268;
		int  decimal = 258;
		List<Integer>  hexList = new ArrayList<Integer>();
		System.out.print("Decimal of " + decimal + " equals to hex of 0x");
		while (true) {
			int remainder = decimal % 16;
			hexList.add (remainder);

			decimal = decimal / 16;
			if (decimal == 0) {
				break;
			}
		}
		for (int i=hexList.size(); i>0; --i) {
			int  hexNum = hexList.get(i-1).intValue();
            char hexChar = (char) ((hexNum < 10) ? ('0' + hexNum)
                                                 : ('A' + hexNum));
            System.out.print (hexChar);
		}
		System.out.print ("\n");
	}
}
