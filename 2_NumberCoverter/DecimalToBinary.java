import java.util.List;
import java.util.ArrayList;

public class DecimalToBinary {
	public static void main (String args[]) {
		int  decimal = 9;
		List<Integer>  digitList = new ArrayList<Integer>();
		System.out.print("Decimal of " + decimal + " equals to binary of ");
		while (true) {
			int remainder = decimal % 2;
			digitList.add (remainder);

			decimal = decimal / 2;
			if (decimal == 0) {
				break;
			}
		}
		for (int i=digitList.size(); i>0; --i) {
			int  digit = digitList.get(i-1).intValue();
            System.out.print (digit);
		}
		System.out.print ("b\n");
	}
}
